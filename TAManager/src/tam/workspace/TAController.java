package tam.workspace;

import tam.jtps.jTPS_Transaction;
import tam.jtps.jTPS;
import djf.controller.AppFileController;
import static djf.settings.AppPropertyType.*;
import djf.ui.AppGUI;
import static tam.TAManagerProp.*;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.util.HashMap;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.*;

import javafx.scene.layout.Pane;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.style.TAStyle;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_CELL;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN;
import static tam.style.TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE;
import tam.workspace.TAWorkspace;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import tam.TAManagerProp;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.GridPane;
import tam.jtps.AddTA_Transaction;
import tam.jtps.jTPS;
import tam.jtps.jTPS_Transaction;
import tam.jtps.*;
import tam.jtps.AddTAToGrid_Transaction;
import tam.jtps.DeleteTA_Transaction;
import javafx.scene.layout.GridPane;
import java.util.*;


/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @author Timothy Hart
 * @version 1.0
 */
public class TAController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;
    private static final String EMAIL_PATTERN =
		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private Pattern pattern = Pattern.compile(EMAIL_PATTERN);
    private Matcher matcher;
    boolean addNewTA = true;
    TeachingAssistant editedTA;
    boolean valid = true;
    static jTPS jTPS = new jTPS();
    
    /**
     * Constructor, note that the app must already be constructed.
     */
    public TAController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        
    }
    
    /**
     * This helper method should be called every time an edit happens.
     */    
    private void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }
    
    /**
     * This method responds to when the user requests to add
     * a new TA via the UI. Note that it must first do some
     * validation to make sure a unique name and email address
     * has been provided.
     */
    public void handleAddTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData)app.getDataComponent();
        ObservableList<TeachingAssistant> allTAs;  
        allTAs = data.getAllTAs();
        editedTA = null;
        addNewTA = true;
        
        allTAs.forEach((TA) -> {
           if(TA.getBeingEdited()){
               addNewTA = false;
               editedTA = (TeachingAssistant)TA;
           }
        });
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));            
        }
        // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (email.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));                        
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else 
        {   jTPS_Transaction transaction = new AddTA_Transaction(app, name, email, (TeachingAssistant)workspace.getTATable().getSelectionModel().getSelectedItem());
            if (data.containsTA(name, email)) {
            if(!addNewTA){
                
                valid = true;
                allTAs.forEach((TA) -> {
                    if(TA.getBeingEdited() == false){
                        if(TA.getName().equalsIgnoreCase(name) || TA.getEmail().equalsIgnoreCase(email)){
                            valid = false;
                        }
                    }
                });
                    
                if(valid){
                    boolean isValidEmail = false;
                    isValidEmail = EmailValidator(email);
                    if(isValidEmail){
                        // ADD THE NEW TA TO THE DATA
                        jTPS.addTransaction(transaction);
                        jTPS.doTransaction();
                        
                        // CLEAR THE TEXT FIELDS
                        nameTextField.setText("");
                        emailTextField.setText("");
            
                        // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
                        nameTextField.requestFocus();
            
                        // WE'VE CHANGED STUFF
                        markWorkAsEdited();
                    }
                    else{
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show(props.getProperty(INVALID_EMAIL_TITLE), props.getProperty(INVALID_EMAIL_MESSAGE));
                    }
                }
                else{
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));
                }
            }
            else{
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));
            }
        }
        
        // EVERYTHING IS FINE, ADD A NEW TA
        else {
            boolean isValidEmail = false;
            isValidEmail = EmailValidator(email);
           
            if(isValidEmail){
                // ADD THE NEW TA TO THE DATA
                jTPS.addTransaction(transaction);
                jTPS.doTransaction();
            
                // CLEAR THE TEXT FIELDS
                nameTextField.setText("");
                emailTextField.setText("");
            
                // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
                nameTextField.requestFocus();
            
                // WE'VE CHANGED STUFF
                markWorkAsEdited();
                
                if(editedTA != null){
                    addNewTA = true;
                    editedTA.setBeingEdited(false);
                }
            }
            else{
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_EMAIL_TITLE), props.getProperty(INVALID_EMAIL_MESSAGE));
            }
        }}
    }
    
    public boolean EmailValidator(String email){
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * This function provides a response for when the user presses a
     * keyboard key. Note that we're only responding to Delete, to remove
     * a TA.
     * 
     * @param e the KeyEvent
     */
    public void handleKeyPress(KeyEvent e) {
        // DID THE USER PRESS THE DELETE KEY?
        
        if (e.getCode() == KeyCode.DELETE) {
            TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
            TableView taTable = workspace.getTATable();
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            jTPS_Transaction transaction = new DeleteTA_Transaction(app, (TeachingAssistant)(selectedItem));
            jTPS.addTransaction(transaction);
            jTPS.doTransaction();
        }
        if(e.getCode() == KeyCode.Z && e.isControlDown()){
            jTPS.undoTransaction();
            markWorkAsEdited();
        }
        
        if(e.getCode() == KeyCode.Y && e.isControlDown()){
            jTPS.doTransaction();
            markWorkAsEdited();
        }
    }
    
    public void handleSelectedTA(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            ObservableList<TeachingAssistant> allTAs;
            TAData data = (TAData)app.getDataComponent();
        
            allTAs = data.getAllTAs();
            
            allTAs.forEach((TA) -> {
                TA.setBeingEdited(false);
            });
            
            TeachingAssistant ta = (TeachingAssistant)selectedItem;
            workspace.getNameTextField().setText(ta.getName());
            workspace.getEmailTextField().setText(ta.getEmail());
            ta.setBeingEdited(true);
            
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            workspace.getAddButton().setText(props.getProperty(TAManagerProp.UPDATE_BUTTON_TEXT.toString()));
            
        }
    }

    /**
     * This function provides a response for when the user clicks
     * on the office hours grid to add or remove a TA to a time slot.
     * 
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            // GET THE TA
            TeachingAssistant ta = (TeachingAssistant)selectedItem;
            String taName = ta.getName();
            TAData data = (TAData)app.getDataComponent();
            String cellKey = pane.getId();
            
            // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
            jTPS_Transaction transaction = new AddTAToGrid_Transaction(app, cellKey, taName);
            jTPS.addTransaction(transaction);
            jTPS.doTransaction();
            
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }
    
    void handleGridCellMouseExited(Pane pane) {
        String cellKey = pane.getId();
        TAData data = (TAData)app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();

        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }
    }

    void handleGridCellMouseEntered(Pane pane) {
        String cellKey = pane.getId();
        TAData data = (TAData)app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        
        // THE MOUSED OVER PANE
        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_CELL);
        
        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }
    }
    
    public void handleClear(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        if(ta != null){
            ta.setBeingEdited(false);
            taTable.getSelectionModel().clearSelection();
        }
        workspace.getNameTextField().setText("");
        workspace.getEmailTextField().setText("");
        workspace.getNameTextField().requestFocus();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        workspace.getAddButton().setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
    }
    
    public void handleChangeStartHour(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        ComboBox comboBox = workspace.getStartComboBox();
        int selectedHour = comboBox.getSelectionModel().getSelectedIndex();
        TAData data = (TAData)app.getDataComponent();
        ObservableList<String> timeOptions = workspace.getTimeOptions();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        System.out.println("Selected Hour " + selectedHour);
        System.out.println("Current Hour "+ data.getStartHour());
        
        if(selectedHour > data.getEndHour()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_START_HOUR_TITLE), props.getProperty(INVALID_START_HOUR_MESSAGE));
            comboBox.getSelectionModel().clearSelection();
        }
        
        else if(selectedHour > data.getMin()){
            jTPS_Transaction transaction = new ChangeStartHour_Transaction(app, data, selectedHour);
            jTPS.addTransaction(transaction);
            jTPS.doTransaction();
        }
    }
        
    public void handleChangeEndHour(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        ComboBox comboBox = workspace.getEndComboBox();
        int selectedHour = comboBox.getSelectionModel().getSelectedIndex();
        TAData data = (TAData)app.getDataComponent();
        ObservableList<String> timeOptions = workspace.getTimeOptions();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        if(selectedHour < data.getStartHour()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_END_HOUR_TITLE), props.getProperty(INVALID_END_HOUR_MESSAGE));
        }
        else if(selectedHour < data.getMax()){
            jTPS_Transaction transaction = new ChangeEndHour_Transaction(app, data, selectedHour);
            jTPS.addTransaction(transaction);
            jTPS.doTransaction();
        }
    }
}