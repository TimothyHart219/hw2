/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.jtps;

import djf.ui.AppGUI;
import java.util.Collections;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.workspace.TAWorkspace;
import javafx.scene.layout.Pane;
import tam.TAManagerApp;

/**
 *
 * @author tjhha
 */
public class AddTAToGrid_Transaction implements jTPS_Transaction {
    
    TAManagerApp app;
    String cellKey;
    String taName;
    
    public AddTAToGrid_Transaction(TAManagerApp app, String cellKey, String taName){
        this.cellKey = cellKey;
        this.taName = taName;
        this.app = app;
    }
    
    @Override
    public void doTransaction(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        
        StringProperty cellProp = data.getOfficeHours().get(cellKey);
        String cellText = cellProp.getValue();

        // IF IT ALREADY HAS THE TA, REMOVE IT
        if (cellText.contains(taName)) {
            String[] names = cellText.split("\n");
            boolean insertTA = true;
            for(int i = 0; i < names.length; i++){
                if(taName.equals(names[i])){
                    data.removeTAFromCell(cellProp, taName);
                    insertTA = false;
                    break;
                }
            }
            if(insertTA)
                cellProp.setValue(cellText + "\n" + taName);
        } // OTHERWISE ADD IT
        else if (cellText.length() == 0) {
            cellProp.setValue(taName);
        } else {
            cellProp.setValue(cellText + "\n" + taName);
        }
        
    }
    @Override
    public void undoTransaction(){
        doTransaction();
    }
}
