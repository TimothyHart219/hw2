package tam.jtps;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import djf.ui.AppGUI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import properties_manager.PropertiesManager;
import tam.TAManagerProp;
import tam.jtps.jTPS_Transaction;
import tam.data.TeachingAssistant;
import tam.data.TAData;
import tam.workspace.TAWorkspace;
import tam.TAManagerApp;

/**
 *
 * @author tjhha
 */
public class AddTA_Transaction implements jTPS_Transaction {
    
    String initName;
    String initEmail;
    TAManagerApp app;
    TeachingAssistant ta;
    TeachingAssistant selectedTA;
    String oldName;
    String oldEmail;
    boolean remove = true;
    ArrayList<StringProperty> oldOfficeHourProps = new ArrayList();
    ArrayList<String> oldOfficeHourData = new ArrayList();
    TeachingAssistant editedTA;
    
    public AddTA_Transaction (TAManagerApp app, String initName, String initEmail, TeachingAssistant selectedTA){
        this.initName = initName;
        this.initEmail = initEmail;
        this.app = app;
        this.selectedTA = selectedTA;  
    }
    
    @Override
    public void doTransaction(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();

        TableView taTable = workspace.getTATable();
        
        if(selectedTA == null){
            // MAKE THE TA
            ta = new TeachingAssistant(initName, initEmail);
            

            // ADD THE TA
            if (!data.containsTA(initName, initEmail)) {
                data.getTeachingAssistants().add(ta);
            }
        
            // SORT THE TAS
            Collections.sort(data.getTeachingAssistants());
        }
        else{
            oldName = selectedTA.getName();
            oldEmail = selectedTA.getEmail();
            List<String> keys = new ArrayList<String>(data.getOfficeHours().keySet());
            for(int row = 1; row < 2*(data.getEndHour() - data.getStartHour())+1; row++){
                for(int col = 2; col < 7; col++){
                    String cellKey = data.getCellKey(col, row);
                    StringProperty cellProp = data.getOfficeHours().get(cellKey);
                
                    if(cellProp != null){
                        String cellText = cellProp.getValue();
                        if(data.boolToggleTAOfficeHours(cellKey, selectedTA.getName())){
                            oldOfficeHourProps.add(cellProp);
                            oldOfficeHourData.add(cellText);
                        }
                    }
                }
            }
            
            for(int i = 0; i < keys.size(); i++){
                // GET THE CELL PROP & CELL TEXT
                StringProperty cellProp = data.getOfficeHours().get(keys.get(i));
                String cellText = cellProp.getValue();
                String[] names = cellText.split("\n");
                String newCellText = "";
                
                for(int j = 0; j < names.length; j++){
                    if(names[j].equalsIgnoreCase(selectedTA.getName())){
                        names[j] = initName;
                    }
                }
                for(int j = 0; j < names.length; j++){
                    if(j == names.length - 1)
                        newCellText += names[j];
                    else
                        newCellText += names[j]+"\n";
                }
                cellProp.setValue(newCellText);          
            } 
            
            taTable.getSelectionModel().clearSelection();
            editedTA = selectedTA;
            data.removeTA(selectedTA.getName());
            selectedTA = null;
            // MAKE THE TA
            ta = new TeachingAssistant(initName, initEmail);

            // ADD THE TA
            if (!data.containsTA(initName, initEmail)) {
                data.getTeachingAssistants().add(ta);
            }
        
            // SORT THE TAS
            Collections.sort(data.getTeachingAssistants());

            PropertiesManager props = PropertiesManager.getPropertiesManager();
            workspace.getAddButton().setText(props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString()));
        }
        
    }
    @Override
    public void undoTransaction(){
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        TAData data = (TAData)app.getDataComponent();
        if(editedTA != null){
            // IS A TA SELECTED IN THE TABLE?
 

            // GET THE TA AND REMOVE IT
        
            data.removeTA(ta.getName());
                
            // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
            List<String> keys = new ArrayList<String>(data.getOfficeHours().keySet());
            for(int i = 0; i < keys.size(); i++){
                // GET THE CELL PROP & CELL TEXT
                StringProperty cellProp = data.getOfficeHours().get(keys.get(i));
                String cellText = cellProp.getValue();
                String[] names = cellText.split("\n");
                String newCellText = "";
                
                for(int j = 0; j < names.length; j++){
                    if(names[j].equalsIgnoreCase(ta.getName())){
                        names[j] = oldName;
                    }
                }
                for(int j = 0; j < names.length; j++){
                    if(j == names.length - 1)
                        newCellText += names[j];
                    else
                        newCellText += names[j]+"\n";
                }
                cellProp.setValue(newCellText);          
            }
            selectedTA = new TeachingAssistant(oldName, oldEmail);

            // ADD THE TA
            if (!data.containsTA(oldName, oldEmail)) {
                data.getTeachingAssistants().add(selectedTA);
            }
        
            // SORT THE TAS
            Collections.sort(data.getTeachingAssistants());
        }
        else{
            // ADD THE TA
        
            data.removeTA(ta.getName());

            // SORT THE TAS
            Collections.sort(data.getTeachingAssistants());

            
        }
        
        // WE'VE CHANGED STUFF
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);    
    }
}
