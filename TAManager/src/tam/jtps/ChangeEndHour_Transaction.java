package tam.jtps;
import static djf.settings.AppPropertyType.END_DELETE_GRID_DATA_MESSAGE;
import static djf.settings.AppPropertyType.END_DELETE_GRID_DATA_TITLE;
import static djf.settings.AppPropertyType.START_DELETE_GRID_DATA_MESSAGE;
import static djf.settings.AppPropertyType.START_DELETE_GRID_DATA_TITLE;
import djf.ui.AppGUI;
import djf.ui.AppYesNoCancelDialogSingleton;
import javafx.beans.property.StringProperty;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.*;
import tam.workspace.*;

/**
 *
 * @author Timothy Hart
 */
public class ChangeEndHour_Transaction implements jTPS_Transaction {
    
    TAManagerApp app;
    TAData data;
    int selectedHour;
    int originalStart;
    int originalEnd;
    int rowsAdded;
    String[][] gridData;
    
    public ChangeEndHour_Transaction(TAManagerApp app, TAData data, int selectedHour){
        this.app = app;
        this.selectedHour = selectedHour;
        if(selectedHour <0) this.selectedHour = 0;
        this.data = data;
        this.originalStart = data.getStartHour();
        this.originalEnd = data.getEndHour();
    }
    
    @Override
    public void doTransaction(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        if(data.checkConflictEnd(selectedHour)){
            AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
            yesNoDialog.show(props.getProperty(END_DELETE_GRID_DATA_TITLE), props.getProperty(END_DELETE_GRID_DATA_MESSAGE));
            String selection = yesNoDialog.getSelection();
            

            if(selection.equals(AppYesNoCancelDialogSingleton.YES)){
                //remove data
                gridData = new String[5][(data.getEndHour()-data.getStartHour())*2+1];
                
                rowsAdded = data.getEndHour() - selectedHour;
                int hourDiff = data.getEndHour() - selectedHour;
                for(int row = 1; row < (data.getEndHour()-data.getStartHour())*2+1; row++){
                    for(int col = 2; col < 7; col++){
                        String cellKey = data.getCellKey(col, row);
                        StringProperty cellProp = data.getOfficeHours().get(cellKey);
                        String cellText = cellProp.getValue();
                        gridData[col-2][row] = cellText;
                        String[] names= cellText.split("\n");
                        data.setCellProperty(col, row, cellProp);
                        for(int i = 0; i < names.length; i++){
                            data.toggleTAOfficeHours(cellKey, names[i]);
                           
                        }
                       
                    }
                }                
                
                data.initHours(Integer.toString(data.getStartHour()), Integer.toString(selectedHour));
                workspace.reloadOfficeHoursGrid(data);
                
                for(int row = 1; row < (selectedHour-data.getStartHour())*2+1; row++){
                    for(int col = 2; col < 7; col++){
                        String cellKey = data.getCellKey(col, row);
                        StringProperty cellProp = data.getOfficeHours().get(cellKey);
                        cellProp.setValue(gridData[col-2][row]);
                    }
               }  
            }
        }
        
        else if(data.getEndHour() > selectedHour){               
            gridData = new String[5][(data.getEndHour()-data.getStartHour())*2+1];
                
                rowsAdded = data.getEndHour() - selectedHour;
                int hourDiff = data.getEndHour() - selectedHour;
                for(int row = 1; row < (data.getEndHour()-data.getStartHour())*2+1; row++){
                    for(int col = 2; col < 7; col++){
                        String cellKey = data.getCellKey(col, row);
                        StringProperty cellProp = data.getOfficeHours().get(cellKey);
                        String cellText = cellProp.getValue();
                        gridData[col-2][row] = cellText;
                        String[] names= cellText.split("\n");
                        data.setCellProperty(col, row, cellProp);
                        for(int i = 0; i < names.length; i++){
                            data.toggleTAOfficeHours(cellKey, names[i]);
                           
                        }
                       
                    }
                }                
                
                data.initHours(Integer.toString(data.getStartHour()), Integer.toString(selectedHour));
                workspace.reloadOfficeHoursGrid(data);
                
                for(int row = 1; row < (selectedHour-data.getStartHour())*2+1; row++){
                    for(int col = 2; col < 7; col++){
                        String cellKey = data.getCellKey(col, row);
                        StringProperty cellProp = data.getOfficeHours().get(cellKey);
                        cellProp.setValue(gridData[col-2][row]);
                    }
               }    
        }
        //selected hour > end hour
        else{
        gridData = new String[5][(data.getEndHour()-data.getStartHour())*2+1];
                if(selectedHour > data.getMax()) selectedHour = data.getMax();
                rowsAdded = data.getEndHour() - selectedHour;
                int hourDiff = selectedHour-data.getEndHour();
                for(int row = 1; row < (data.getEndHour()-data.getStartHour())*2+1; row++){
                    for(int col = 2; col < 7; col++){
                        String cellKey = data.getCellKey(col, row);
                        StringProperty cellProp = data.getOfficeHours().get(cellKey);
                        String cellText = "";
                        if(cellProp != null){
                            cellText = cellProp.getValue();
                            gridData[col-2][row] = cellText;
                            if(cellText.contains("\n")){
                                String[] names= cellText.split("\n");
                                data.setCellProperty(col, row, cellProp);
                                for(int i = 0; i < names.length; i++){
                                    data.toggleTAOfficeHours(cellKey, names[i]);
                           
                                }
                            }
                        }
                       
                    }
                }                
                
                int holder = data.getEndHour();
                data.initHours(Integer.toString(data.getStartHour()), Integer.toString(selectedHour));
                
                for(int row = 1; row < (holder-data.getStartHour())*2+1; row++){
                    for(int col = 2; col < 7; col++){
                        String cellKey = data.getCellKey(col, row);
                        StringProperty cellProp = data.getOfficeHours().get(cellKey);
                        cellProp.setValue(gridData[col-2][row]);
                    }
               } 
        }
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui); 
    }
    
    @Override
    public void undoTransaction(){
        
            data.initHours(Integer.toString(data.getStartHour()), Integer.toString(selectedHour+rowsAdded));
            for(int row = 0; row < (originalEnd-originalStart)*2; row++){
                for(int col = 2; col < 7; col++){
                    String cellKey = data.getCellKey(col, row);
                    StringProperty cellProp = data.getOfficeHours().get(cellKey);
                    cellProp.setValue(gridData[col-2][row]);
                }
            }
            AppGUI gui = app.getGUI();
            gui.getFileController().markAsEdited(gui); 
        
    
    }
}