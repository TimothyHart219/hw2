package tam.jtps;
import djf.ui.AppGUI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import properties_manager.PropertiesManager;
import tam.TAManagerProp;
import tam.jtps.jTPS_Transaction;
import tam.data.TeachingAssistant;
import tam.data.TAData;
import tam.workspace.TAWorkspace;
import tam.TAManagerApp;
import java.util.HashMap;

/**
 *
 * @author Timothy Hart
 */
public class DeleteTA_Transaction implements jTPS_Transaction {
    
    String initName;
    String initEmail;
    TAManagerApp app;
    TeachingAssistant ta;
    ArrayList<StringProperty> officeHourProps = new ArrayList();
    ArrayList<String> officeHourData = new ArrayList();
    
    public DeleteTA_Transaction (TAManagerApp app, TeachingAssistant ta){
        this.app = app;
        this.ta = ta;
    }
    
    
    @Override
    public void doTransaction(){
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        
        // IS A TA SELECTED IN THE TABLE?
        initName = ta.getName();
        initEmail = ta.getEmail();
       System.out.println(initName);

        // GET THE TA AND REMOVE IT
        TAData data = (TAData)app.getDataComponent();
                
        // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
        //
        
        for(int row = 1; row < 2*(data.getEndHour() - data.getStartHour())+1; row++){
            for(int col = 2; col < 7; col++){
                String cellKey = data.getCellKey(col, row);
                StringProperty cellProp = data.getOfficeHours().get(cellKey);
                
                if(cellProp != null){
                    String cellText = cellProp.getValue();
                    if(data.boolToggleTAOfficeHours(cellKey, initName)){
                        System.out.println("hi");
                        officeHourProps.add(cellProp);
                        officeHourData.add(cellText);
                    }
                }
            }
        }
            
        HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();
        for (Label label : labels.values()) {
            if (label.getText().equals(initName)
                || (label.getText().contains(initName + "\n"))
                || (label.getText().contains("\n" + initName))) {
                    data.removeTAFromCell(label.textProperty(), initName);
                }
            }
        
        data.removeTA(initName);
            // WE'VE CHANGED STUFF
            AppGUI gui = app.getGUI();
            gui.getFileController().markAsEdited(gui);    
    }
      
    @Override
    public void undoTransaction(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        TAData data = (TAData)app.getDataComponent();
        
        TableView taTable = workspace.getTATable();
        
        
        // MAKE THE TA
         ta = new TeachingAssistant(initName, initEmail);

        // ADD THE TA
        if (!data.containsTA(initName, initEmail)) {
            data.getTeachingAssistants().add(ta);
        }

        // SORT THE TAS
        Collections.sort(data.getTeachingAssistants());

        for(int i = 0; i < officeHourProps.size(); i++){
            StringProperty cellProp = officeHourProps.get(i);
            cellProp.setValue(officeHourData.get(i));
        }
        
    }
}

