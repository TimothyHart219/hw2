
package tam.jtps;
import static djf.settings.AppPropertyType.START_DELETE_GRID_DATA_MESSAGE;
import static djf.settings.AppPropertyType.START_DELETE_GRID_DATA_TITLE;
import djf.ui.AppGUI;
import djf.ui.AppYesNoCancelDialogSingleton;
import javafx.beans.property.StringProperty;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.data.*;
import tam.workspace.*;

/**
 *
 * @author Timothy Hart
 */
public class ChangeStartHour_Transaction implements jTPS_Transaction {
    
    TAManagerApp app;
    TAData data;
    int selectedHour;
    int originalStart;
    int originalEnd;
    int rowsAdded;
    String[][] gridData;
    
    public ChangeStartHour_Transaction(TAManagerApp app, TAData data, int selectedHour){
        this.app = app;
        this.selectedHour = selectedHour;
        if(selectedHour <0) this.selectedHour = 0;
        this.data = data;
        this.originalStart = data.getStartHour();
        this.originalEnd = data.getEndHour();
    }
    
    @Override
    public void doTransaction(){
        if(data.checkConflictStart(selectedHour)){
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
            yesNoDialog.show(props.getProperty(START_DELETE_GRID_DATA_TITLE), props.getProperty(START_DELETE_GRID_DATA_MESSAGE));
            String selection = yesNoDialog.getSelection();
            TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
            
            if(selection.equals(AppYesNoCancelDialogSingleton.YES)){
                //remove data
                gridData = new String[5][(data.getEndHour()-data.getStartHour())*2+1];
                
                rowsAdded = data.getStartHour() - selectedHour;
                int hourDiff = selectedHour - data.getStartHour();
                for(int row = (data.getEndHour()-data.getStartHour())*2; row > 0; row--){
                    for(int col = 2; col < 7; col++){
                        String cellKey = data.getCellKey(col, row);
                        StringProperty cellProp = data.getOfficeHours().get(cellKey);
                        String cellText = cellProp.getValue();
                        gridData[col-2][row] = cellText;
                        String[] names= cellText.split("\n");
                        data.setCellProperty(col, row+hourDiff*2, cellProp);
                        for(int i = 0; i < names.length; i++){
                            data.toggleTAOfficeHours(cellKey, names[i]);
                           
                        }
                       
                    }
                }                
                
                data.initHours(Integer.toString(selectedHour), Integer.toString(data.getEndHour()));
                workspace.reloadOfficeHoursGrid(data);
                
                for(int row = (data.getEndHour()-data.getStartHour())*2; row > 0; row--){
                    for(int col = 2; col < 7; col++){
                        String cellKey = data.getCellKey(col, row);
                        StringProperty cellProp = data.getOfficeHours().get(cellKey);
                        cellProp.setValue(gridData[col-2][row+hourDiff*2]);
                    }
               }  
            }
        }
        else if(data.getStartHour() < selectedHour){
            gridData = new String[5][(data.getEndHour()-data.getStartHour())*2+1];
                rowsAdded = data.getStartHour() - selectedHour; 
            int hourDiff = selectedHour - data.getStartHour();
            for(int row = (data.getEndHour()-data.getStartHour())*2; row > 0; row--){
                for(int col = 2; col < 7; col++){
                    String cellKey = data.getCellKey(col, row);
                    StringProperty cellProp = data.getOfficeHours().get(cellKey);
                    String cellText = cellProp.getValue();
                    gridData[col-2][row] = cellText;
                    String[] names= cellText.split("\n");
                    for(int i = 0; i < names.length; i++){
                        data.toggleTAOfficeHours(cellKey, names[i]);
                    }
                       
                }
            }  
            data.initHours(Integer.toString(selectedHour), Integer.toString(data.getEndHour()));
            for(int row = (data.getEndHour()-data.getStartHour())*2; row > 0; row--){
                for(int col = 2; col < 7; col++){
                    String cellKey = data.getCellKey(col, row);
                    StringProperty cellProp = data.getOfficeHours().get(cellKey);
                    cellProp.setValue(gridData[col-2][row+hourDiff*2]);
                }
            }  
        }else{
             rowsAdded = data.getStartHour() - selectedHour;
            gridData = new String[5][(data.getEndHour()-data.getStartHour())*2+1];
            if(selectedHour < data.getMin()) selectedHour = data.getMin();
            int hourDiff = data.getStartHour()-selectedHour;
            if(hourDiff != 0){
                for(int row = (data.getEndHour()-data.getStartHour())*2; row > 0; row--){
                    for(int col = 2; col < 7; col++){
                        String cellKey = data.getCellKey(col, row);
                        StringProperty cellProp = data.getOfficeHours().get(cellKey);
                        String cellText = cellProp.getValue();
                        gridData[col-2][row] = cellText;
                        String[] names= cellText.split("\n");
                        for(int i = 0; i < names.length; i++){
                            data.toggleTAOfficeHours(cellKey, names[i]);
                        }   
                    }
                }
            
                data.initHours(Integer.toString(selectedHour), Integer.toString(data.getEndHour()));
                for(int row = (data.getEndHour()-data.getStartHour())*2; row > hourDiff*2; row--){
                    for(int col = 2; col < 7; col++){
                        String cellKey = data.getCellKey(col, row);
                        StringProperty cellProp = data.getOfficeHours().get(cellKey);
                        cellProp.setValue(gridData[col-2][row-hourDiff*2]);
                    }
                }
            }
        }
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui); 
    }
    
    @Override
    public void undoTransaction(){
        if(rowsAdded > 0){
            data.initHours(Integer.toString(selectedHour+rowsAdded), Integer.toString(data.getEndHour()));
            for(int row = (originalEnd-originalStart)*2; row > 0; row--){
                for(int col = 2; col < 7; col++){
                    String cellKey = data.getCellKey(col, row);
                    StringProperty cellProp = data.getOfficeHours().get(cellKey);
                    cellProp.setValue(gridData[col-2][row]);
                }
            }
        }
        else{
            data.initHours(Integer.toString(selectedHour+rowsAdded), Integer.toString(data.getEndHour()));
            for(int row = (originalEnd-originalStart)*2; row > 0; row--){
                for(int col = 2; col < 7; col++){
                    String cellKey = data.getCellKey(col, row);
                    StringProperty cellProp = data.getOfficeHours().get(cellKey);
                    cellProp.setValue(gridData[col-2][row]);
                }
            }
        }
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui); 
    }
}
